﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClassEdit;
using OntologyAppDBConnector;
using Ontology_Module;
using OntologyClasses.BaseClasses;

namespace OntologyClassEditTester
{
    public partial class Form1 : Form
    {
        private Globals globals;
        private UserControl_ClassEdit userControl_ClassEdit;
        private dlg_Attribute_Long dlgAttributeLong;
        private frmGraph frmGraph;

        private clsOntologyItem classItem;

        public Form1()
        {
            InitializeComponent();
            globals = new Globals();
            userControl_ClassEdit = new UserControl_ClassEdit(globals);
            userControl_ClassEdit._getAttributeType += ClassEditForm_getAttributeType;
            userControl_ClassEdit._getLongValue += UserControl_ClassEdit_getLongValue;
            userControl_ClassEdit._getClass += UserControl_ClassEdit_getClass;
            userControl_ClassEdit._getRelationType += UserControl_ClassEdit_getRelationType;
            userControl_ClassEdit._openGraph += UserControl_ClassEdit_openGraph; ;
            userControl_ClassEdit.Dock = DockStyle.Fill;
            this.Controls.Add(userControl_ClassEdit);

            classItem = new OntologyClasses.BaseClasses.clsOntologyItem
            {
                GUID = "c8aa7b6a8336491899adf2bf6ea89c34",
                Name = "XXX_Test1",
                GUID_Parent = globals.Root.GUID,
                Type = globals.Type_Class
            };

            userControl_ClassEdit.Initialize_ClassEdit(classItem);

        }

        private void UserControl_ClassEdit_openGraph(List<clsClassAtt> ClassAttributes, List<clsClassRel> ClassRelations_Conscious, List<clsClassRel> ClassRelations_Subconscious, List<clsClassRel> ClassRelations_Other)
        {
            frmGraph = new frmGraph(globals);

            frmGraph.Initialize_Lists();
            frmGraph.OList_Classes.Add(classItem);

            if (ClassAttributes != null)
            {
                frmGraph.ClassAtts = ClassAttributes;
            }

            if (ClassRelations_Conscious != null)
            {
                var classRelations = ClassRelations_Conscious.GroupBy(clsRel => new { GUID = clsRel.ID_Class_Right, Name = clsRel.Name_Class_Right, Type = globals.Type_Class }).Select(clsRel => new clsOntologyItem { GUID = clsRel.Key.GUID,
                    Name = clsRel.Key.Name,
                    Type = clsRel.Key.Type }).ToList();

                frmGraph.OList_Classes.AddRange(from clsItemNew in classRelations
                                                join clsItemIn in frmGraph.OList_Classes on clsItemNew.GUID equals clsItemIn.GUID into clsItemsIn
                                                from clsItemIn in clsItemsIn.DefaultIfEmpty()
                                                where clsItemIn == null
                                                select clsItemNew);

                frmGraph.OList_ClassRel = ClassRelations_Conscious;
            }

            if (ClassRelations_Subconscious != null)
            {
                var classRelations = ClassRelations_Subconscious.GroupBy(clsRel => new { GUID = clsRel.ID_Class_Left, Name = clsRel.Name_Class_Left, Type = globals.Type_Class }).Select(clsRel => new clsOntologyItem
                {
                    GUID = clsRel.Key.GUID,
                    Name = clsRel.Key.Name,
                    Type = clsRel.Key.Type
                }).ToList();

                frmGraph.OList_Classes.AddRange(from clsItemNew in classRelations
                                                join clsItemIn in frmGraph.OList_Classes on clsItemNew.GUID equals clsItemIn.GUID into clsItemsIn
                                                from clsItemIn in clsItemsIn.DefaultIfEmpty()
                                                where clsItemIn == null
                                                select clsItemNew);

                frmGraph.OList_ClassRel.AddRange(ClassRelations_Subconscious);
            }

            if (ClassRelations_Other != null)
            {
                
                frmGraph.OList_ClassRel.AddRange(ClassRelations_Other);
            }

            frmGraph.Initialize_ListGraph();
            frmGraph.Show();
        }

        private void UserControl_ClassEdit_getRelationType(string sessionId)
        {
            var frmMain = new frmMain(globals, globals.Type_RelationType);
            frmMain.ShowDialog(this);
            if (frmMain.DialogResult == DialogResult.OK)
            {
                userControl_ClassEdit.SetRelationType(frmMain.OList_Simple.FirstOrDefault(), sessionId);
            }
        }

        private void UserControl_ClassEdit_getClass(string sessionId)
        {
            var frmMain = new frmMain(globals, globals.Type_Class);
            frmMain.ShowDialog(this);
            if (frmMain.DialogResult == DialogResult.OK)
            {
                userControl_ClassEdit.SetClass(frmMain.OList_Simple.FirstOrDefault(), sessionId);
            }
        }

        private void UserControl_ClassEdit_getLongValue(string sessionId, long value)
        {
            dlgAttributeLong = new dlg_Attribute_Long("Get Value", globals, value);
            dlgAttributeLong.ShowDialog(this);
            if (dlgAttributeLong.DialogResult == DialogResult.OK)
            {
                userControl_ClassEdit.SetLongValue(sessionId, dlgAttributeLong.Value);
            }
        }

        private void ClassEditForm_getAttributeType(string sessionId)
        {
            var frmMain = new frmMain(globals, globals.Type_AttributeType);
            frmMain.ShowDialog(this);
            if (frmMain.DialogResult == DialogResult.OK)
            {
                userControl_ClassEdit.SetSimpleItemListAttributeType(frmMain.Type_Applied, frmMain.OList_Simple, sessionId);
            }

        }
    }
}
