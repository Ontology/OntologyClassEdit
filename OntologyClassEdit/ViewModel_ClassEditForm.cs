﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit
{
    public class ViewModel_ClassEditForm : NotifyPropertyChange
    {
        private Globals globals;

        private clsOntologyItem classItem;

        private ClassViewItem classViewItem;
        public ClassViewItem ClassViewItem
        {
            get { return classViewItem; }
            set
            {
                classViewItem = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ClassViewItem);
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ClassGuid);
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ClassName);
            }
        }

        private string labelClassName = "Class:";
        public string LabelClassName
        {
            get { return labelClassName; }
            set
            {
                labelClassName = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassName);
            }
        }


        public string ClassName
        {
            get { return ClassViewItem != null ? ClassViewItem.ClassName : null; }
            set
            {
                ClassViewItem.DoTransaction = !IsReadOnly_Name;
                if (ClassViewItem != null)
                {
                    ClassViewItem.ClassName = value;
                }
                
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ClassName);
            }
        }

        private string labelClassGuid = "Guid:";
        public string LabelClassGuid
        {
            get { return labelClassGuid; }
            set
            {
                labelClassGuid = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassGuid);
            }
        }

        public string ClassGuid
        {
            get { return classViewItem != null ? classViewItem.ClassGuid : null; }
            set
            {
                ClassViewItem.DoTransaction = !IsReadOnly_Guid;
                if (classViewItem != null)
                {
                    ClassViewItem.ClassGuid = value;
                }
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ClassGuid);

            }
        }


        private string labelClassAttributes = "Class-Attributes";
        public string LabelClassAttributes
        {
            get { return labelClassAttributes; }
            set
            {
                labelClassAttributes = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassAttributes);
            }
        }

        private string labelClassRelationsForw = "Class-Relations (forw)";
        public string LabelClassRelationsForw
        {
            get { return labelClassRelationsForw; }
            set
            {
                labelClassRelationsForw = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassRelationsForw);
            }
        }

        private string labelClassRelationsBackw = "Class-Relations (backw)";
        public string LabelClassRelationsBackw
        {
            get { return labelClassRelationsBackw; }
            set
            {
                labelClassRelationsBackw = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassRelationsBackw);
            }
        }

        private string labelClassRelationsOr = "Class-Relations (Or)";
        public string LabelClassRelationsOr
        {
            get { return labelClassRelationsOr; }
            set
            {
                labelClassRelationsOr = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelClassRelationsOr);
            }
        }

        private string connectionString = "-";
        public string ConnectionString
        {
            get { return connectionString; }
            set
            {
                connectionString = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_ConnectionString);
            }
        }

        private string labelConnectionString = "Database:";
        public string LabelConnectionString
        {
            get { return labelConnectionString; }
            set
            {
                labelConnectionString = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelConnectionString);
            }
        }

        private string labelForwardRelations = "Forward-Relations";
        public string LabelForwardRelations
        {
            get { return labelForwardRelations; }
            set
            {
                labelForwardRelations = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelForwardRelations);
            }
        }

        private string labelBackwardRelations = "Backward-Relations";
        public string LabelBackwardRelations
        {
            get { return labelBackwardRelations; }
            set
            {
                labelBackwardRelations = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelBackwardRelations);
            }
        }

        private string labelOrRelations = "Object-References";
        public string LabelOrRelations
        {
            get { return labelOrRelations; }
            set
            {
                labelOrRelations = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelOrRelations);
            }
        }

        private string labelView = "Class-Edit";
        public string LabelView
        {
            get { return labelView; }
            set
            {
                labelView = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_LabelView);
            }
        }

        private bool isReadOnly_Name;
        public bool IsReadOnly_Name
        {
            get { return isReadOnly_Name; }
            set
            {
                isReadOnly_Name = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsReadOnly_Name);
            }
        }

        private bool isReadOnly_Guid;
        public bool IsReadOnly_Guid
        {
            get { return isReadOnly_Guid; }
            set
            {
                isReadOnly_Guid = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsReadOnly_Guid);
            }
        }

        private bool isChecked_ClassAttributes;
        public bool IsChecked_ClassAttributes
        {
            get { return isChecked_ClassAttributes; }
            set
            {
                isChecked_ClassAttributes = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsChecked_ClassAttributes);
            }
        }

        
        private bool isChecked_ClassRelationsforw;
        public bool IsChecked_ClassRelationsforw
        {
            get { return isChecked_ClassRelationsforw; }
            set
            {
                isChecked_ClassRelationsforw = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsforw);
            }
        }

        private bool isChecked_ClassRelationsbackw;
        public bool IsChecked_ClassRelationsbackw
        {
            get { return isChecked_ClassRelationsbackw; }
            set
            {
                isChecked_ClassRelationsbackw = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsbackw);
            }
        }

        private bool isChecked_ClassRelationsOR;
        public bool IsChecked_ClassRelationsOR
        {
            get { return isChecked_ClassRelationsOR; }
            set
            {
                isChecked_ClassRelationsOR = value;
                RaisePropertyChanged(NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsOR);
            }
        }

        public ViewModel_ClassEditForm(Globals globals)
        {
            this.globals = globals;
            ConnectionString = globals.Index + "@" + globals.Server + ":" + globals.Port.ToString();

        }

        public clsOntologyItem DeleteClass()
        {
            return ClassViewItem.DeleteClass();
        }

        public void Initialize(clsOntologyItem classItem)
        {
            this.classItem = classItem;

            IsChecked_ClassAttributes = true;
            IsChecked_ClassRelationsbackw = true;
            IsChecked_ClassRelationsforw = true;
            IsChecked_ClassRelationsOR = true;

            IsReadOnly_Name = true;
            ClassViewItem = new ClassViewItem(globals, classItem);
            IsReadOnly_Name = false;
            IsReadOnly_Guid = true;
        }
    }
}
