﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.Translations
{
    public static class TranslationManager
    {
        public static string Message_AttributeTypesFromMainNotPossible
        {
            get
            {
                return "Die Attributtypen konnten nicht geladen werden!";
            }
        }

        public static string Caption_AttributeTypesFromMainNotPossible
        {
            get
            {
                return "Ladefehler!";
            }
        }

        public static string Message_TypeOrCoundWasWrong
        {
            get
            {
                return "Bitte nur einen AttributTypen auswählen!";
            }
        }

        public static string Caption_TypeOrCountWasWrong
        {
            get
            {
                return "Falsche Auswahl";
            }
        }

        public static string Message_ClassAttCannotBeSaved
        {
            get
            {
                return "Der Attributtyp konnte nicht hinzu gefügt werden!";
            }
        }

        public static string Caption_ClassAttCannotBeSaved
        {
            get
            {
                return "Fehler beim Speichern";
            }
        }

        public static string Message_WrongType
        {
            get
            {
                return "Sie haben den falschen Typ ausgewählt!";
            }
        }

        public static string Caption_WrongType
        {
            get
            {
                return "Falscher Typ";
            }
        }

        public static string Message_MinChangeNotValid
        {
            get
            {
                return "Die Änderung des Min-Wertes ist nicht möglich";
            }
        }

        public static string Caption_MinChangeNotValid
        {
            get
            {
                return "Min-Wert Änderung";
            }
        }

        public static string Message_DeleteError
        {
            get
            {
                return "Fehler beim Löschen der Elemente: {0} Elemente zu löschen, {1} Elemente gelöscht, {2} fehlerhafte Löschversuche, {3} Elemente in aktiven Beziehungen.";
            }
        }

        public static string Message_RelationItemsFromMainNotPossible
        {
            get
            {
                return "Die Elemente zur Herstellung einer Beziehung konnten nicht geladen werden!";
            }
        }

        public static string Caption_RelationItemsFromMainNotPossible
        {
            get
            {
                return "Fehler beim Elemente laden!";
            }
        }

        public static string Caption_DeleteError
        {
            get
            {
                return "Fehler beim Löschen";
            }
        }

        public static string Caption_RelationOfClass
        {
            get
            {
                return "Klasse in Beziehung";
            }
        }

        public static string Message_RelationOfClass
        {
            get
            {
                return "Die Klasse ist noch mindestens an einer Beziehung beteiligt.";
            }
        }

        public static string Caption_ErrorDeletingClass
        {
            get
            {
                return "Fehler beim Löschen der Klasse";
            }
        }

        public static string Message_ErrorDeletingClass
        {
            get
            {
                return "Beim Löschen der Klasse ist ein Fehler aufgetreten.";
            }
        }
    }
}
