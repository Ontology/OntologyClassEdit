﻿using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.ClassAttributes;
using OntologyClassEdit.Translations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.ExceptionHandling
{
    public static class ViewMessageAdapter
    {
        public static List<ViewMessage> ViewMessages { get; set; }
        public static void Convert(ClassRelationsViewModelErrors classAttributeViewModelErrors)
        {
            ViewMessages = new List<ViewMessage>();
            if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.SaveError))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.SaveError,
                    Message = TranslationManager.Message_ClassAttCannotBeSaved,
                    Caption = TranslationManager.Caption_ClassAttCannotBeSaved
                });
            }
            else if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.SaveItems))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.SaveItems,
                    Message = TranslationManager.Message_ClassAttCannotBeSaved,
                    Caption = TranslationManager.Caption_ClassAttCannotBeSaved
                });
            }
            else if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.SelectionCountError))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.SelectionCountError,
                    Message = TranslationManager.Message_TypeOrCoundWasWrong,
                    Caption = TranslationManager.Caption_TypeOrCountWasWrong
                });
            }
            else if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.TypeOrCount))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.TypeOrCount,
                    Message = TranslationManager.Message_TypeOrCoundWasWrong,
                    Caption = TranslationManager.Caption_TypeOrCountWasWrong
                });
            }
            else if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.WrongItemType))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.WrongItemType,
                    Message = TranslationManager.Message_WrongType,
                    Caption = TranslationManager.Caption_WrongType
                });
            }
            else if (classAttributeViewModelErrors.HasFlag(ClassRelationsViewModelErrors.MinForwChangeNotValid))
            {
                ViewMessages.Add(new ViewMessage
                {
                    ClassRelationsViewModelError = ClassRelationsViewModelErrors.MinForwChangeNotValid,
                    Message = TranslationManager.Message_MinChangeNotValid,
                    Caption = TranslationManager.Caption_MinChangeNotValid
                });
            }
        }
    }
}
