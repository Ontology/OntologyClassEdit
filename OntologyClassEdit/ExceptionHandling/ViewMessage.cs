﻿using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.ClassAttributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.ExceptionHandling
{
    public class ViewMessage
    {
        public ClassRelationsViewModelErrors ClassRelationsViewModelError;
        public string Message { get; set; }
        public string Caption { get; set; }
    }
}
