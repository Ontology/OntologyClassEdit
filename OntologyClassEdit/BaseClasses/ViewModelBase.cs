﻿using OntologyAppDBConnector;
using OntologyClassEdit.Converter;
using OntologyClassEdit.ExceptionHandling;
using OntologyClasses.BaseClasses;
using OntologyViewModels;
using OntologyViewModels.BaseClasses;
using OntologyViewModels.DataAdapter;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.BaseClasses
{
    [Flags]
    public enum ClassRelationsViewModelErrors
    {
        None = 0,
        SaveItems = 1,
        TypeOrCount = 2,
        WrongItemType = 4,
        SelectionCountError = 8,
        SaveError = 16,
        MinForwChangeNotValid = 32,
        MaxForwChangeNotValid = 64,
        MaxBackwChangeNotValid = 128
    }
    public abstract class ViewModelBase : NotifyPropertyChange
    {
        public clsOntologyItem ClassItem { get; set; }

        internal Globals globals;

        public ViewModelWinFormsOItemList ViewModelOitemList { get; internal set; }

        private bool isEnabled_Add;
        public bool IsEnabled_Add
        {
            get
            {
                return isEnabled_Add;
            }
            set
            {
                isEnabled_Add = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_Add);
            }
        }

        private bool isEnabled_Del;
        public bool IsEnabled_Del
        {
            get
            {
                return isEnabled_Del;
            }
            set
            {
                isEnabled_Del = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_Del);
            }
        }

        private bool isEnabled_MinForwTo0;
        public bool IsEnabled_MinForwTo0
        {
            get
            {
                return isEnabled_MinForwTo0;
            }
            set
            {
                isEnabled_MinForwTo0 = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MinForwTo0);
            }
        }

        private bool isEnabled_MinForwDec;
        public bool IsEnabled_MinForwDec
        {
            get
            {
                return isEnabled_MinForwDec;
            }
            set
            {
                isEnabled_MinForwDec = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MinForwDec);
            }
        }

        private bool isEnabled_MinForwInc;
        public bool IsEnabled_MinForwInc
        {
            get
            {
                return isEnabled_MinForwInc;
            }
            set
            {
                isEnabled_MinForwInc = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MinForwInc);
            }
        }

        private bool isEnabled_MinForwEdit;
        public bool IsEnabled_MinForwEdit
        {
            get
            {
                return isEnabled_MinForwEdit;
            }
            set
            {
                isEnabled_MinForwEdit = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MinForwEdit);
            }
        }

        private bool isEnabled_MaxForwToInvinite;
        public bool IsEnabled_MaxForwToInvinite
        {
            get
            {
                return isEnabled_MaxForwToInvinite;
            }
            set
            {
                isEnabled_MaxForwToInvinite = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MaxForwToInvinite);
            }
        }

        private bool isEnabled_MaxForwDec;
        public bool IsEnabled_MaxForwDec
        {
            get
            {
                return isEnabled_MaxForwDec;
            }
            set
            {
                isEnabled_MaxForwDec = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MaxForwDec);
            }
        }

        private bool isEnabled_MaxForwInc;
        public bool IsEnabled_MaxForwInc
        {
            get
            {
                return isEnabled_MaxForwInc;
            }
            set
            {
                isEnabled_MaxForwInc = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MaxForwInc);
            }
        }

        private bool isEnabled_MaxForwEdit;
        public bool IsEnabled_MaxForwEdit
        {
            get
            {
                return isEnabled_MaxForwEdit;
            }
            set
            {
                isEnabled_MaxForwEdit = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MaxForwEdit);
            }
        }

        private bool isEnabled_MaxBackwEdit;
        public bool IsEnabled_MaxBackwEdit
        {
            get
            {
                return isEnabled_MaxBackwEdit;
            }
            set
            {
                isEnabled_MaxBackwEdit = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_IsEnabled_MaxBackwEdit);
            }
        }

        private string label_Count;
        public string Label_Count
        {
            get { return label_Count; }
            set
            {
                label_Count = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_Label_Count);
            }
        }

        private int countItems;
        public int CountItems
        {
            get { return countItems; }
            set
            {
                countItems = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_CountItems);
            }
        }

        public string CountItemsString
        {
            get { return CountItems.ToString(); }
        }

        private ImageType loadStateImageType;
        public ImageType LoadStateImageType
        {
            get
            {
                return loadStateImageType;
            }
            set
            {
                loadStateImageType = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_LoadStateImageType);
            }
        }


        public List<ViewMessage> ViewMessages
        {
            get { return ViewMessageAdapter.ViewMessages; }

        }

        private ClassRelationsViewModelErrors viewModelErrors;
        public ClassRelationsViewModelErrors ViewModelErrors
        {
            get { return viewModelErrors; }
            set
            {
                viewModelErrors = value;
                ViewMessageAdapter.Convert(viewModelErrors);
                RaisePropertyChanged(NotifyChanges.ClassRelation_ViewModelErrors);
                RaisePropertyChanged(NotifyChanges.ClassRelation_ViewMessages);
            }
        }

        public ViewModelBase(Globals globals)
        {
            this.globals = globals;
        }

        public long? GetMinForw()
        {
            var result = ViewModelOitemList.GetMin();
            return result;
        }

        public long? GetMaxForw()
        {
            var result = ViewModelOitemList.GetMax();
            return result;
        }

        public long? GetMaxBackw()
        {
            var result = ViewModelOitemList.GetMaxBackw();
            return result;
        }

        public ClassRelationsViewModelErrors DecMinForw()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.DecMin());
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors IncMinForw()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.IncMin());
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors SetMinForwTo0()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.SetMinTo0());
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors SetMinForwToValue(long value)
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.SetMinToValue(value));
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors SetMaxForwToValue(long value)
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.SetMaxToValue(value));
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors SetMaxBackwToValue(long value)
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.SetMaxBackwToValue(value));
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors SetMaxForwToInfinite()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.SetMaxToInfinite());
            return ViewModelErrors;
        }


        public ClassRelationsViewModelErrors DecMaxForw()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.DecMax());
            return ViewModelErrors;
        }

        public ClassRelationsViewModelErrors IncMaxForw()
        {
            ViewModelErrors = ViewModelErrorTypeToClassRelationsError.Convert(ViewModelOitemList.IncMax());
            return ViewModelErrors;
        }

        private void CleanUp()
        {

            IsEnabled_Add = false;
            IsEnabled_Del = false;
            IsEnabled_MaxForwInc = false;
            IsEnabled_MinForwInc = false;
            IsEnabled_MaxForwToInvinite = false;
            IsEnabled_MinForwTo0 = false;
            IsEnabled_MaxForwDec = false;
            IsEnabled_MinForwDec = false;
            IsEnabled_MaxForwEdit = false;
            IsEnabled_MinForwEdit = false;
            IsEnabled_MaxBackwEdit = false;
        }

        public void Initialize(clsOntologyItem classItem)
        {
            ClassItem = classItem;
            CleanUp();
            ViewModelOitemList = new ViewModelWinFormsOItemList(globals);
            ViewModelOitemList.PropertyChanged += ViewModelOitemList_PropertyChanged;

            //RefreshGrid();
        }


        private void ViewModelOitemList_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {

            if (e.PropertyName == PropertyName.OItemList_ViewModel_ListLoadState)
            {
                LoadStateImageType = LoadStateToImageConverter.ConvertListFilterStateToImageType(ViewModelOitemList != null ? ViewModelOitemList.ListLoadState : ListLoadStateItem.None);

            }
            else if (e.PropertyName == PropertyName.OItemList_ListAdapter_Result_ListLoader
                && ViewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.ListLoadLoaded)
                && !ViewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.ListLoadPending)
                && !ViewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterLoaded)
                && !ViewModelOitemList.ListLoadState.HasFlag(ListLoadStateItem.AdvancedFilterPending))
            {
                //advancedFilter = true;
                //viewModelOitemList.Initialize_AdvancedFilter(advancedFilter_Direction: globals.Direction_LeftRight, advancedFilter_Class: new clsOntologyItem { GUID = "6f9bf5b4c942465a8c788a7ac8224112" });

            }
            else if (e.PropertyName == PropertyName.OItemList_CountItems)
            {
                CountItems = ViewModelOitemList.CountItems;
            }
            else if (e.PropertyName == PropertyName.OItemList_DelAllowed)
            {
                IsEnabled_Del = ViewModelOitemList.DelAllowed;
            }
            else if (e.PropertyName == PropertyName.OItemList_AddAllowed)
            {
                IsEnabled_Add = ViewModelOitemList.AddAllowed;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMaxIncrease)
            {
                IsEnabled_MaxForwInc = ViewModelOitemList.IsAllowedMaxIncrease;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMinIncrease)
            {
                IsEnabled_MinForwInc = ViewModelOitemList.IsAllowedMinIncrease;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMaxToInvinite)
            {
                IsEnabled_MaxForwToInvinite = ViewModelOitemList.IsAllowedMaxToInvinite;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMinDecrease)
            {
                IsEnabled_MinForwTo0 = ViewModelOitemList.IsAllowedMinTo0;
                IsEnabled_MinForwDec = ViewModelOitemList.IsAllowedMinDecrease;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMaxDecrease)
            {
                IsEnabled_MaxForwDec = ViewModelOitemList.IsAllowedMaxDecrease;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMaxEdit)
            {
                IsEnabled_MaxForwEdit = ViewModelOitemList.IsAllowedMaxEdit;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMaxBackwEdit)
            {
                IsEnabled_MaxBackwEdit = ViewModelOitemList.IsAllowedMaxBackwEdit;
            }
            else if (e.PropertyName == PropertyName.OItemList_IsAllowedMinEdit)
            {
                IsEnabled_MinForwEdit = ViewModelOitemList.IsAllowedMinEdit;
            }

        }

    }
}
