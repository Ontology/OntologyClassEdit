﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit
{
    public class ClassViewItem : NotifyPropertyChange
    {
        private clsTransaction transaction;

        private clsOntologyItem classItemOrigin = null;

        private clsOntologyItem resultTransaction;
        public clsOntologyItem ResultTransaction
        {
            get { return resultTransaction; }
            set
            {
                resultTransaction = value;
                RaisePropertyChanged(Properties.Resources.ClassViewItem_ResultTransaction);
            }
        }

        private bool doTransaction;
        public bool DoTransaction
        {
            get { return doTransaction; }
            set
            {
                doTransaction = value;
                RaisePropertyChanged(Properties.Resources.ClassViewItem_DoTransaction);
            }
        }

        private clsOntologyItem classItem;
        public clsOntologyItem ClassItem
        {
            get { return classItem; }
            set
            {
                classItem = value;
                classItemOrigin = classItem.Clone();
                RaisePropertyChanged(Properties.Resources.ClassViewItem_ClassItem);
            }
        }

        public string classGuid;
        public string ClassGuid
        {
            get { return classItem.GUID; }
            set
            {
                classItem.GUID = value;
                CheckTransaction();
                RaisePropertyChanged(Properties.Resources.ClassViewItem_ClassGuid);
            }
        }

        public string className;
        public string ClassName
        {
            get { return classItem.Name; }
            set
            {
                classItem.Name = value;
                CheckTransaction();
                RaisePropertyChanged(Properties.Resources.ClassViewItem_ClassName);
            }
        }


        private void CheckTransaction()
        {
            if (classItemOrigin == null)
            {
                classItemOrigin = ClassItem.Clone();
                return;
            }
            if (classItem.GUID != classItemOrigin.GUID) return;
            
            
            if (doTransaction && (classItem.Name != classItemOrigin.Name || classItem.GUID_Parent != classItemOrigin.GUID_Parent))
            {
                transaction.ClearItems();
                ResultTransaction = transaction.do_Transaction(classItem);

            }

            classItemOrigin = classItem.Clone();
        }

        public clsOntologyItem DeleteClass()
        {
            transaction.ClearItems();
            return transaction.do_Transaction(classItem, false, true);
        }

        public void Undo()
        {
            classItem.GUID = classItemOrigin.GUID;
            classItem.Name = classItemOrigin.Name;
            classItem.GUID_Parent = classItemOrigin.GUID_Parent;
        }

        public ClassViewItem(Globals globals, clsOntologyItem classItem)
        {
            transaction = new clsTransaction(globals);
            this.classItem = classItem;
        }
    }
}
