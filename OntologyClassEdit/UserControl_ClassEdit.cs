﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyClassEdit.Translations;
using OntologyClassEdit.ClassAttributes;
using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntologyClassEdit.ClassRelations;

namespace OntologyClassEdit
{
    public partial class UserControl_ClassEdit : UserControl
    {
        private Globals globals;
        private clsOntologyItem classItem;

        private ViewModel_ClassEditForm viewModelClassEditForm;
        private UserControl_ClassAttributeTypes userControlClassAttributeTypes;
        private UserControl_ClassRelations userControl_ClassRelations_Conscious;
        private UserControl_ClassRelations userControl_ClassRelations_Subconscious;
        private UserControl_ClassRelations userControl_ClassRelations_Other;

        public List<clsClassAtt> ClassAttributes
        {
            get
            {
                return userControlClassAttributeTypes.ClassAttributes;
            }
        }

        public List<clsClassRel> ClassRelationsConscious
        {
            get
            {
                return userControl_ClassRelations_Conscious.ClassRelations;
            }
        }

        public List<clsClassRel> ClassRelationsSubConscious
        {
            get
            {
                return userControl_ClassRelations_Subconscious.ClassRelations;
            }
        }

        public List<clsClassRel> ClassRelationsOther
        {
            get
            {
                return userControl_ClassRelations_Other.ClassRelations;
            }
        }

        private delegate void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e);

        public delegate void GetAttributeType(string sessionId);
        public event GetAttributeType _getAttributeType;

        public delegate void GetClass(string sessionId);
        public event GetClass _getClass;

        public delegate void GetRelationType(string sessionId);
        public event GetRelationType _getRelationType;

        public delegate void DeletedItem(clsOntologyItem oItem);
        public event DeletedItem _deletedItem;

        public delegate void GetLongValue(string sessionId, long value);
        public event GetLongValue _getLongValue;

        public delegate void OpenGraph(List<clsClassAtt> ClassAttributes, 
            List<clsClassRel> ClassRelations_Conscious,
            List<clsClassRel> ClassRelations_Subconscious,
            List<clsClassRel> ClassRelations_Other);
        public event OpenGraph _openGraph;

        private string sessionId_ClassAttributes;
        private string sessionId_RelationConscious;
        private string sessionId_RelationSubconscious;
        private string sessionId_RelationOther;

        public void SetSimpleItemListAttributeType(string typeApplied, List<clsOntologyItem> simpleList, string sessionId)
        {
            userControlClassAttributeTypes.SetSimpleItemList(typeApplied, simpleList, sessionId);
        }

        public void SetLongValue(string sessionId, long value)
        {
            if (sessionId == sessionId_ClassAttributes)
            {
                userControlClassAttributeTypes.SetLongValue(value, sessionId);
            }
            else if (sessionId == sessionId_RelationConscious)
            {
                userControl_ClassRelations_Conscious.SetLongValue(value, sessionId);
            }
            else if (sessionId == sessionId_RelationSubconscious)
            {
                userControl_ClassRelations_Subconscious.SetLongValue(value, sessionId);
            }
            else if (sessionId == sessionId_RelationOther)
            {
                userControl_ClassRelations_Other.SetLongValue(value, sessionId);
            }
            
        }

        public void SetClass(clsOntologyItem classItem, string sessionId)
        {
            if (sessionId == sessionId_RelationConscious)
            {
                userControl_ClassRelations_Conscious.SetClass(classItem, sessionId);
            }
            else if (sessionId == sessionId_RelationSubconscious)
            {
                userControl_ClassRelations_Subconscious.SetClass(classItem, sessionId);
            }
            else if (sessionId == sessionId_RelationOther)
            {
                userControl_ClassRelations_Other.SetClass(classItem, sessionId);
            }
            
        }

        public void SetRelationType(clsOntologyItem relationTypeItem, string sessionId)
        {
            if (sessionId == sessionId_RelationConscious)
            {
                userControl_ClassRelations_Conscious.SetRelationType(relationTypeItem, sessionId);
            }
            else if (sessionId == sessionId_RelationSubconscious)
            {
                userControl_ClassRelations_Subconscious.SetRelationType(relationTypeItem, sessionId);
            }
            else if (sessionId == sessionId_RelationOther)
            {
                userControl_ClassRelations_Other.SetRelationType(relationTypeItem, sessionId);
            }
        }

        private void ConfigureControls()
        {
            TabPage_Backward.Text = viewModelClassEditForm.LabelBackwardRelations;
            ToolStripMenuItem_ClassAttributes.Text = viewModelClassEditForm.LabelClassAttributes;
            ToolStripLabel_GUIDLBL.Text = viewModelClassEditForm.LabelClassGuid;
            ToolStripLabel_ClassLBL.Text = viewModelClassEditForm.LabelClassName;
            ClassRelationsbackwToolStripMenuItem.Text = viewModelClassEditForm.LabelClassRelationsBackw;
            ClassRelationsforwToolStripMenuItem.Text = viewModelClassEditForm.LabelClassRelationsForw;
            ClassRelationsORToolStripMenuItem.Text = viewModelClassEditForm.LabelClassRelationsOr;
            ToolStripStatusLabel_DatabaseLBL.Text = viewModelClassEditForm.LabelConnectionString;
            ToolStripStatusLabel_Database.Text = viewModelClassEditForm.ConnectionString;
            TabPage_Forward.Text = viewModelClassEditForm.LabelForwardRelations;
            TabPage_ObjectReferences.Text = viewModelClassEditForm.LabelOrRelations;
            this.Text = viewModelClassEditForm.LabelView;
        }

        private void Initialize()
        {
            userControlClassAttributeTypes = new UserControl_ClassAttributeTypes(globals);
            userControlClassAttributeTypes.Dock = DockStyle.Fill;
            SplitContainer1.Panel1.Controls.Add(userControlClassAttributeTypes);

            userControlClassAttributeTypes.getAttributeType += UserControlClassAttributeTypes_getAttributeType;
            userControlClassAttributeTypes.getLongValue += UserControlClassAttributeTypes_getLongValue;

            userControl_ClassRelations_Conscious = new UserControl_ClassRelations(globals);
            userControl_ClassRelations_Conscious.Dock = DockStyle.Fill;
            TabPage_Forward.Controls.Add(userControl_ClassRelations_Conscious);

            userControl_ClassRelations_Conscious.getClass += UserControl_ClassRelations_Conscious_getClass;
            userControl_ClassRelations_Conscious.getRelationType += UserControl_ClassRelations_Conscious_getRelationType;
            userControl_ClassRelations_Conscious.getLongValue += UserControl_ClassRelations_Conscious_getLongValue;

            userControl_ClassRelations_Subconscious = new UserControl_ClassRelations(globals);
            userControl_ClassRelations_Subconscious.Dock = DockStyle.Fill;
            TabPage_Backward.Controls.Add(userControl_ClassRelations_Subconscious);

            userControl_ClassRelations_Subconscious.getClass += UserControl_ClassRelations_Subconscious_getClass;
            userControl_ClassRelations_Subconscious.getRelationType += UserControl_ClassRelations_Subconscious_getRelationType;
            userControl_ClassRelations_Subconscious.getLongValue += UserControl_ClassRelations_Subconscious_getLongValue;

            userControl_ClassRelations_Other = new UserControl_ClassRelations(globals);
            userControl_ClassRelations_Other.Dock = DockStyle.Fill;
            TabPage_ObjectReferences.Controls.Add(userControl_ClassRelations_Other);

            userControl_ClassRelations_Other.getClass += UserControl_ClassRelations_Other_getClass;
            userControl_ClassRelations_Other.getRelationType += UserControl_ClassRelations_Other_getRelationType;
            userControl_ClassRelations_Other.getLongValue += UserControl_ClassRelations_Other_getLongValue;

            ConfigureControls();
            
        }

        private void UserControl_ClassRelations_Other_getLongValue(string sessionId, long value)
        {
            if (_getLongValue != null)
            {
                sessionId_RelationOther = sessionId;
                _getLongValue(sessionId, value);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Subconscious_getLongValue(string sessionId, long value)
        {
            if (_getLongValue != null)
            {
                sessionId_RelationSubconscious = sessionId;
                _getLongValue(sessionId, value);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Conscious_getLongValue(string sessionId, long value)
        {
            if (_getLongValue != null)
            {
                sessionId_RelationConscious = sessionId;
                _getLongValue(sessionId, value);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Other_getRelationType(string sessionId)
        {
            sessionId_RelationOther = sessionId;
            if (_getRelationType != null)
            {
                _getRelationType(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Other_getClass(string sessionId)
        {
            sessionId_RelationOther = sessionId;
            if (_getClass != null)
            {
                _getClass(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Subconscious_getRelationType(string sessionId)
        {
            sessionId_RelationSubconscious = sessionId;
            if (_getRelationType != null)
            {
                _getRelationType(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Subconscious_getClass(string sessionId)
        {
            sessionId_RelationSubconscious = sessionId;
            if (_getClass != null)
            {
                _getClass(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Conscious_getRelationType(string sessionId)
        {
            sessionId_RelationConscious = sessionId;
            if (_getRelationType != null)
            {
                _getRelationType(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControl_ClassRelations_Conscious_getClass(string sessionId)
        {
            sessionId_RelationConscious = sessionId;
            if (_getClass != null)
            {
                _getClass(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_RelationItemsFromMainNotPossible, TranslationManager.Caption_RelationItemsFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControlClassAttributeTypes_getLongValue(string sessionId, long value)
        {
            if (_getLongValue != null)
            {
                sessionId_ClassAttributes = sessionId;
                _getLongValue(sessionId, value);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void UserControlClassAttributeTypes_getAttributeType(string sessionId)
        {
            if (_getAttributeType != null)
            {
                _getAttributeType(sessionId);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void ViewModelClassEditForm_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                var deleg = new ViewModelPropertyChanged(ViewModelClassEditForm_PropertyChanged);
                this.Invoke(deleg, sender, e);
            }
            else
            {
                if (e.PropertyName == NotifyChanges.ClassEditProperty_ConnectionString)
                {
                    ToolStripStatusLabel_Database.Text = viewModelClassEditForm.ConnectionString;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsReadOnly_Guid)
                {
                    ToolStripTextBox_GUID.ReadOnly = viewModelClassEditForm.IsReadOnly_Guid;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsReadOnly_Name)
                {
                    ToolStripTextBox_GUID.ReadOnly = viewModelClassEditForm.IsReadOnly_Name;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsReadOnly_Guid)
                {
                    ToolStripTextBox_GUID.ReadOnly = viewModelClassEditForm.IsReadOnly_Guid;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_ClassGuid)
                {
                    ToolStripTextBox_GUID.Text = viewModelClassEditForm.ClassGuid;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_ClassName)
                {
                    ToolStripTextBox_Name.Text = viewModelClassEditForm.ClassName;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsChecked_ClassAttributes)
                {
                    ToolStripMenuItem_ClassAttributes.Checked = viewModelClassEditForm.IsChecked_ClassAttributes;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsbackw)
                {
                    ClassRelationsbackwToolStripMenuItem.Checked = viewModelClassEditForm.IsChecked_ClassRelationsbackw;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsforw)
                {
                    ClassRelationsforwToolStripMenuItem.Checked = viewModelClassEditForm.IsChecked_ClassRelationsforw;
                }
                else if (e.PropertyName == NotifyChanges.ClassEditProperty_IsChecked_ClassRelationsOR)
                {
                    ClassRelationsORToolStripMenuItem.Checked = viewModelClassEditForm.IsChecked_ClassRelationsOR;
                }
            }
        }

        private void ToolStripTextBox_Name_TextChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.ClassName = ToolStripTextBox_Name.Text;
        }

        private void ToolStripTextBox_GUID_TextChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.ClassGuid = ToolStripTextBox_GUID.Text;
        }

        public UserControl_ClassEdit(Globals globals)
        {
            InitializeComponent();
            viewModelClassEditForm = new ViewModel_ClassEditForm(globals);
            viewModelClassEditForm.PropertyChanged += ViewModelClassEditForm_PropertyChanged;
            this.globals = globals;
            this.classItem = classItem;

            Initialize();
        }

        public void Initialize_ClassEdit(clsOntologyItem classItem)
        {
            userControlClassAttributeTypes.Initialize(classItem);
            userControl_ClassRelations_Conscious.Initialize(classItem, OntologyViewModels.OItemList.ListType.ClassClassRelation_Conscious);
            userControl_ClassRelations_Subconscious.Initialize(classItem, OntologyViewModels.OItemList.ListType.ClassClassRelation_Subconscious);
            userControl_ClassRelations_Other.Initialize(classItem, OntologyViewModels.OItemList.ListType.ClassOtherRelation_Conscious);


            viewModelClassEditForm.Initialize(classItem);
        }

        private void ToolStripButton_DelClass_Click(object sender, EventArgs e)
        {
            var result = viewModelClassEditForm.DeleteClass();

            if (result.GUID == globals.LState_Success.GUID)
            {
                if (_deletedItem != null)
                {
                    _deletedItem(viewModelClassEditForm.ClassViewItem.ClassItem);
                }
            }
            else if (result.GUID == globals.LState_Error.GUID)
            {
                MessageBox.Show(this, TranslationManager.Message_ErrorDeletingClass, TranslationManager.Caption_ErrorDeletingClass, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else if (result.GUID == globals.LState_Relation.GUID)
            {
                MessageBox.Show(this, TranslationManager.Message_RelationOfClass, TranslationManager.Caption_RelationOfClass, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        private void ToolStripMenuItem_ClassAttributes_CheckStateChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.IsChecked_ClassAttributes = ToolStripMenuItem_ClassAttributes.Checked;
        }

        private void ClassRelationsforwToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.IsChecked_ClassRelationsforw = ClassRelationsforwToolStripMenuItem.Checked;
        }

        private void ClassRelationsbackwToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.IsChecked_ClassRelationsbackw = ClassRelationsbackwToolStripMenuItem.Checked;
        }

        private void ClassRelationsORToolStripMenuItem_CheckStateChanged(object sender, EventArgs e)
        {
            viewModelClassEditForm.IsChecked_ClassRelationsOR = ClassRelationsORToolStripMenuItem.Checked;
        }

        private void ToolStripSplitButton_Graph_ButtonClick(object sender, EventArgs e)
        {
            if (_openGraph != null)
            {
                _openGraph(viewModelClassEditForm.IsChecked_ClassAttributes ? userControlClassAttributeTypes.ClassAttributes : null,
                    viewModelClassEditForm.IsChecked_ClassRelationsforw ? userControl_ClassRelations_Conscious.ClassRelations : null,
                    viewModelClassEditForm.IsChecked_ClassRelationsbackw ? userControl_ClassRelations_Subconscious.ClassRelations : null,
                    viewModelClassEditForm.IsChecked_ClassRelationsOR ? userControl_ClassRelations_Other.ClassRelations : null);
            }
        }
    }
}
