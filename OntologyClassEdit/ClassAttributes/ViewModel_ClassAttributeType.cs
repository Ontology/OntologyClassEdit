﻿using OntologyAppDBConnector;
using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.Converter;
using OntologyClassEdit.ExceptionHandling;
using OntologyClassEdit.Translations;
using OntologyClasses.BaseClasses;
using OntologyViewModels;
using OntologyViewModels.BaseClasses;
using OntologyViewModels.DataAdapter;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.ClassAttributes
{
    
    public class ViewModel_ClassAttributeType : OntologyClassEdit.BaseClasses.ViewModelBase
    {

        public List<clsClassAtt> ClassAttributes
        {
            get
            {
                return ViewModelOitemList.ListAdapter.ClassAttViewItems.Select(clsAttItms => clsAttItms.GetProxyItem()).ToList();
            }
        }

        public clsOntologyItem SetSimpleItemList(string typeApplied, List<clsOntologyItem> simpleList)
        {

            var result = globals.LState_Nothing.Clone();
            var addAdapter = ViewModelOitemList.GetAddFormItemType();
            var oItemClass = ViewModelOitemList.ListAdapter.OItem(ViewModelOitemList.ListAdapter.FilterItemClassAtt.ID_Class, globals.Type_Class);

            addAdapter.TypeApplied1 = typeApplied;
            addAdapter.ResultItems_OntologyEditor1 = simpleList;

            if (oItemClass != null && addAdapter.CheckOk())
            {
                result = addAdapter.Add_ClassAttributes(oItemClass);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    ViewModelErrors |= ClassRelationsViewModelErrors.SaveItems;
                }
            }
            else
            {
                ViewModelErrors |= NonNameErrorToClassRelationsViewModelError.convert(addAdapter.NonNameErrorType);
                result = globals.LState_Error.Clone();
            }

            return result;
        }

        public ViewModel_ClassAttributeType(Globals globals) : base(globals)
        {
            
        }

        

        public DeleteCounter DelClassAttributes()
        {
            var result = ViewModelOitemList.DelClassAttributes();
            if (result.CountError != 0 || result.CountRelated != 0)
            {
                result.ErrorCaption = TranslationManager.Caption_DeleteError;
                result.ErrorMessage = string.Format(TranslationManager.Message_DeleteError, result.CountToDo, result.CountDone, result.CountError, result.CountRelated);
            }
            return result;
        }

       
    }   
}
