﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OntologyAppDBConnector;
using OntologyViewModels.OItemList;
using OntologyViewModels;
using OntologyViewModels.DataAdapter;
using OntologyClasses.BaseClasses;
using OntologyClassEdit.Translations;
using OntologyClassEdit.Converter;
using OntologyClassEdit.ExceptionHandling;
using OntologyClassEdit.BaseClasses;

namespace OntologyClassEdit.ClassAttributes
{
    public partial class UserControl_ClassAttributeTypes : UserControl
    {
        private Globals globals;
        
        private clsOntologyItem classItem;

        private ViewModel_ClassAttributeType viewModel;

        private delegate void ChangedViewModelProperty(object sender, PropertyChangedEventArgs e);

        public delegate void GetAttributeType(string sessionId);
        public event GetAttributeType getAttributeType;

        public delegate void GetLongValue(string sessionId, long value);
        public event GetLongValue getLongValue;

        private string currentSessionIdForSimpleList;
        private string currentSessionIdForMinForw;
        private string currentSessionIdForMaxForw;

        public List<clsClassAtt> ClassAttributes
        {
            get
            {
                return viewModel.ClassAttributes;
            }
        }

        public void SetSimpleItemList(string typeApplied, List<clsOntologyItem> simpleList, string sessionId)
        {
            if (currentSessionIdForSimpleList != sessionId) return;

            var result = viewModel.SetSimpleItemList(typeApplied, simpleList);
            if (result.GUID == globals.LState_Error.GUID)
            {
                if (viewModel.ViewModelErrors.HasFlag(ClassRelationsViewModelErrors.TypeOrCount))
                {
                    MessageBox.Show(this, TranslationManager.Message_TypeOrCoundWasWrong, TranslationManager.Caption_TypeOrCountWasWrong, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else if (viewModel.ViewModelErrors.HasFlag(ClassRelationsViewModelErrors.SaveItems))
                {
                    MessageBox.Show(this, TranslationManager.Message_ClassAttCannotBeSaved, TranslationManager.Caption_ClassAttCannotBeSaved, MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                RefreshGrid();
            }
            

        }

        public void SetLongValue(long value, string sessionId)
        {
            if (currentSessionIdForMaxForw != sessionId && currentSessionIdForMinForw != sessionId) return;

            var result = currentSessionIdForMinForw == sessionId ?  viewModel.SetMinForwToValue(value) : viewModel.SetMaxForwToValue(value);

            var errorMessage = viewModel.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.MinForwChangeNotValid);
            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

                if (errorMessage != null)
                {
                    MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    RefreshGrid();
                }
                
            }

        }

        public UserControl_ClassAttributeTypes(Globals globals)
        {
            InitializeComponent();
            this.globals = globals;
            viewModel = new ViewModel_ClassAttributeType(globals);
            viewModel.PropertyChanged += ViewModel_PropertyChanged;
            ConfigureControls();

        }

        public void Initialize(clsOntologyItem classItem)
        {
            this.classItem = classItem;
            viewModel.Initialize(classItem);

            RefreshGrid();
        }

        private void ViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (this.InvokeRequired)
            {
                ChangedViewModelProperty deleg = new ChangedViewModelProperty(ViewModel_PropertyChanged);
                this.Invoke(deleg, sender, e);
            }
            else
            {
                if(e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_Add)
                {
                    ToolStripButton_Add.Enabled = viewModel.IsEnabled_Add;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_Del)
                {
                    ToolStripButton_Del.Enabled = viewModel.IsEnabled_Del;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MaxForwDec)
                {
                    toolStripButton_DecMaxForw.Enabled = viewModel.IsEnabled_MaxForwDec;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MaxForwEdit)
                {
                    toolStripButton_EditMaxForw.Enabled = viewModel.IsEnabled_MaxForwEdit;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MaxForwInc)
                {
                    toolStripButton_IncMaxForw.Enabled = viewModel.IsEnabled_MaxForwInc;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MaxForwToInvinite)
                {
                    toolStripButton_SetMaxToInvinite.Enabled = viewModel.IsEnabled_MaxForwToInvinite;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MinForwDec)
                {
                    toolStripButton_DecMinForw.Enabled = viewModel.IsEnabled_MinForwDec;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MinForwEdit)
                {
                    toolStripButton_EditMinForw.Enabled = viewModel.IsEnabled_MinForwEdit;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MinForwInc)
                {
                    toolStripButton_IncMinForw.Enabled = viewModel.IsEnabled_MinForwInc;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_IsEnabled_MinForwTo0)
                {
                    toolStripButton_SetMinForwTo0.Enabled = viewModel.IsEnabled_MinForwTo0;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_Label_Count)
                {
                    ToolStripLabel_CountLBL.Text = viewModel.Label_Count;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_CountItems)
                {
                    ToolStripLabel_Count.Text = viewModel.CountItemsString;
                }
                else if (e.PropertyName == NotifyChanges.ClassRelation_LoadStateImageType)
                {
                    ConfigureControls();
                }

            }
        }

        private void RefreshGrid()
        {
            if (viewModel.ViewModelOitemList != null)
            {
                viewModel.ViewModelOitemList.WinForms_Initialize_ClassAttListForGridView(new clsClassAtt { ID_Class = classItem.GUID }, DataGridView_AttributeTypes, false);
            }
            else
            {
                DataGridView_AttributeTypes.DataSource = null;
            }
            
        }

        private void ConfigureControls()
        {
            toolStripLabel_List.Image = LoadStateToImageConverter.ConvertImageTypeToImage(viewModel != null ? viewModel.LoadStateImageType : ImageType.NotLoaded);
            ToolStripButton_Add.Enabled = viewModel!= null ? viewModel.IsEnabled_Add : false;
            ToolStripButton_Del.Enabled = viewModel != null ? viewModel.IsEnabled_Del : false;
            toolStripButton_IncMaxForw.Enabled = viewModel != null ? viewModel.IsEnabled_MaxForwInc : false;
            toolStripButton_IncMinForw.Enabled = viewModel != null ? viewModel.IsEnabled_MinForwInc : false;
            toolStripButton_SetMaxToInvinite.Enabled = viewModel != null ? viewModel.IsEnabled_MaxForwToInvinite : false;
            toolStripButton_SetMinForwTo0.Enabled = viewModel != null ? viewModel.IsEnabled_MinForwTo0 : false;
            toolStripButton_DecMaxForw.Enabled = viewModel != null ? viewModel.IsEnabled_MaxForwDec : false;
            toolStripButton_DecMinForw.Enabled = viewModel != null ? viewModel.IsEnabled_MinForwDec : false;


        }

        

        private void ToolStripButton_Add_Click(object sender, EventArgs e)
        {
            if (getAttributeType != null)
            {
                currentSessionIdForSimpleList = globals.NewGUID;
                getAttributeType(currentSessionIdForSimpleList);
            }
            else
            {
                MessageBox.Show(this, TranslationManager.Message_AttributeTypesFromMainNotPossible, TranslationManager.Caption_AttributeTypesFromMainNotPossible, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void toolStripButton_SetMinForwTo0_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.SetMinForwTo0();

            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            RefreshGrid();

        }

        private void toolStripButton_IncMinForw_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.IncMinForw();

            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            RefreshGrid();
        }

        private void toolStripButton_DecMinForw_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.DecMinForw();

            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            RefreshGrid();
        }

        private void toolStripButton_EditMinForw_Click(object sender, EventArgs e)
        {
            if (getLongValue != null)
            {
                var MinForw = viewModel.GetMinForw();

                if (MinForw == null)
                {

                }
                else
                {
                    currentSessionIdForMinForw = globals.NewGUID;
                    getLongValue(currentSessionIdForMinForw, MinForw.Value);
                }
                
            }

            RefreshGrid();
        }

        private void toolStripButton_SetMaxForwToInvinite_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.SetMaxForwToInfinite();
            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

            RefreshGrid();
        }

        private void toolStripButton_DecMaxForw_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.DecMaxForw();

            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.MaxForwChangeNotValid);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

                if (errorMessage != null)
                {
                    MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            RefreshGrid();


        }

        private void toolStripButton_EditMaxForw_Click(object sender, EventArgs e)
        {
            if (getLongValue != null)
            {
                var MaxForw = viewModel.GetMaxForw();

                if (MaxForw == null)
                {

                }
                else
                {
                    currentSessionIdForMaxForw = globals.NewGUID;
                    getLongValue(currentSessionIdForMaxForw, MaxForw.Value);
                }

            }

            RefreshGrid();
        }

        private void toolStripButton_IncMaxForw_Click(object sender, EventArgs e)
        {
            var errorType = viewModel.IncMaxForw();

            var errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.MaxForwChangeNotValid);

            if (errorMessage != null)
            {
                MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                errorMessage = ViewMessageAdapter.ViewMessages.FirstOrDefault(message => message.ClassRelationsViewModelError == ClassRelationsViewModelErrors.SaveError);

                if (errorMessage != null)
                {
                    MessageBox.Show(this, errorMessage.Message, errorMessage.Caption, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }

            RefreshGrid();
        }

        private void ToolStripButton_Del_Click(object sender, EventArgs e)
        {
            var counter = viewModel.DelClassAttributes();
            if (!string.IsNullOrEmpty(counter.ErrorMessage))
            {
                MessageBox.Show(this, counter.ErrorMessage, counter.ErrorCaption, MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

            RefreshGrid();

        }
    }
}
