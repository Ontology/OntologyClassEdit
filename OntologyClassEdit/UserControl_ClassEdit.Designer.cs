﻿namespace OntologyClassEdit
{
    partial class UserControl_ClassEdit
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.StatusStrip1 = new System.Windows.Forms.StatusStrip();
            this.ToolStripStatusLabel_DatabaseLBL = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripStatusLabel_Database = new System.Windows.Forms.ToolStripStatusLabel();
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.SplitContainer1 = new System.Windows.Forms.SplitContainer();
            this.TabControl1 = new System.Windows.Forms.TabControl();
            this.TabPage_Forward = new System.Windows.Forms.TabPage();
            this.TabPage_Backward = new System.Windows.Forms.TabPage();
            this.TabPage_ObjectReferences = new System.Windows.Forms.TabPage();
            this.ToolStrip2 = new System.Windows.Forms.ToolStrip();
            this.ToolStripSplitButton_Graph = new System.Windows.Forms.ToolStripSplitButton();
            this.ToolStripMenuItem_ClassAttributes = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassRelationsforwToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassRelationsbackwToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ClassRelationsORToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripLabel_ClassLBL = new System.Windows.Forms.ToolStripLabel();
            this.ToolStripTextBox_Name = new System.Windows.Forms.ToolStripTextBox();
            this.ToolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.ToolStripLabel_GUIDLBL = new System.Windows.Forms.ToolStripLabel();
            this.ToolStripTextBox_GUID = new System.Windows.Forms.ToolStripTextBox();
            this.ToolStripButton_DelClass = new System.Windows.Forms.ToolStripButton();
            this.StatusStrip1.SuspendLayout();
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.LeftToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).BeginInit();
            this.SplitContainer1.Panel2.SuspendLayout();
            this.SplitContainer1.SuspendLayout();
            this.TabControl1.SuspendLayout();
            this.ToolStrip2.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // StatusStrip1
            // 
            this.StatusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripStatusLabel_DatabaseLBL,
            this.ToolStripStatusLabel_Database});
            this.StatusStrip1.Location = new System.Drawing.Point(0, 534);
            this.StatusStrip1.Name = "StatusStrip1";
            this.StatusStrip1.Size = new System.Drawing.Size(848, 24);
            this.StatusStrip1.TabIndex = 6;
            this.StatusStrip1.Text = "StatusStrip1";
            // 
            // ToolStripStatusLabel_DatabaseLBL
            // 
            this.ToolStripStatusLabel_DatabaseLBL.Name = "ToolStripStatusLabel_DatabaseLBL";
            this.ToolStripStatusLabel_DatabaseLBL.Size = new System.Drawing.Size(58, 19);
            this.ToolStripStatusLabel_DatabaseLBL.Text = "Database:";
            // 
            // ToolStripStatusLabel_Database
            // 
            this.ToolStripStatusLabel_Database.BorderSides = ((System.Windows.Forms.ToolStripStatusLabelBorderSides)((((System.Windows.Forms.ToolStripStatusLabelBorderSides.Left | System.Windows.Forms.ToolStripStatusLabelBorderSides.Top) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Right) 
            | System.Windows.Forms.ToolStripStatusLabelBorderSides.Bottom)));
            this.ToolStripStatusLabel_Database.BorderStyle = System.Windows.Forms.Border3DStyle.SunkenInner;
            this.ToolStripStatusLabel_Database.Name = "ToolStripStatusLabel_Database";
            this.ToolStripStatusLabel_Database.Size = new System.Drawing.Size(16, 19);
            this.ToolStripStatusLabel_Database.Text = "-";
            // 
            // ToolStripContainer1
            // 
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.SplitContainer1);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(815, 509);
            this.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            // 
            // ToolStripContainer1.LeftToolStripPanel
            // 
            this.ToolStripContainer1.LeftToolStripPanel.Controls.Add(this.ToolStrip2);
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.Size = new System.Drawing.Size(848, 534);
            this.ToolStripContainer1.TabIndex = 7;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            // 
            // ToolStripContainer1.TopToolStripPanel
            // 
            this.ToolStripContainer1.TopToolStripPanel.Controls.Add(this.ToolStrip1);
            // 
            // SplitContainer1
            // 
            this.SplitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.SplitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SplitContainer1.Location = new System.Drawing.Point(0, 0);
            this.SplitContainer1.Name = "SplitContainer1";
            // 
            // SplitContainer1.Panel2
            // 
            this.SplitContainer1.Panel2.Controls.Add(this.TabControl1);
            this.SplitContainer1.Size = new System.Drawing.Size(815, 509);
            this.SplitContainer1.SplitterDistance = 403;
            this.SplitContainer1.TabIndex = 0;
            // 
            // TabControl1
            // 
            this.TabControl1.Controls.Add(this.TabPage_Forward);
            this.TabControl1.Controls.Add(this.TabPage_Backward);
            this.TabControl1.Controls.Add(this.TabPage_ObjectReferences);
            this.TabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.TabControl1.Location = new System.Drawing.Point(0, 0);
            this.TabControl1.Name = "TabControl1";
            this.TabControl1.SelectedIndex = 0;
            this.TabControl1.Size = new System.Drawing.Size(404, 505);
            this.TabControl1.TabIndex = 0;
            // 
            // TabPage_Forward
            // 
            this.TabPage_Forward.Location = new System.Drawing.Point(4, 22);
            this.TabPage_Forward.Name = "TabPage_Forward";
            this.TabPage_Forward.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage_Forward.Size = new System.Drawing.Size(396, 479);
            this.TabPage_Forward.TabIndex = 0;
            this.TabPage_Forward.Text = "x_Forward-Relations";
            this.TabPage_Forward.UseVisualStyleBackColor = true;
            // 
            // TabPage_Backward
            // 
            this.TabPage_Backward.Location = new System.Drawing.Point(4, 22);
            this.TabPage_Backward.Name = "TabPage_Backward";
            this.TabPage_Backward.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage_Backward.Size = new System.Drawing.Size(396, 479);
            this.TabPage_Backward.TabIndex = 1;
            this.TabPage_Backward.Text = "x_Backward-Relations";
            this.TabPage_Backward.UseVisualStyleBackColor = true;
            // 
            // TabPage_ObjectReferences
            // 
            this.TabPage_ObjectReferences.Location = new System.Drawing.Point(4, 22);
            this.TabPage_ObjectReferences.Name = "TabPage_ObjectReferences";
            this.TabPage_ObjectReferences.Padding = new System.Windows.Forms.Padding(3);
            this.TabPage_ObjectReferences.Size = new System.Drawing.Size(396, 479);
            this.TabPage_ObjectReferences.TabIndex = 2;
            this.TabPage_ObjectReferences.Text = "x_Object-References";
            this.TabPage_ObjectReferences.UseVisualStyleBackColor = true;
            // 
            // ToolStrip2
            // 
            this.ToolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripSplitButton_Graph});
            this.ToolStrip2.Location = new System.Drawing.Point(0, 3);
            this.ToolStrip2.Name = "ToolStrip2";
            this.ToolStrip2.Size = new System.Drawing.Size(33, 53);
            this.ToolStrip2.TabIndex = 0;
            // 
            // ToolStripSplitButton_Graph
            // 
            this.ToolStripSplitButton_Graph.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripSplitButton_Graph.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripMenuItem_ClassAttributes,
            this.ClassRelationsforwToolStripMenuItem,
            this.ClassRelationsbackwToolStripMenuItem,
            this.ClassRelationsORToolStripMenuItem});
            this.ToolStripSplitButton_Graph.Image = global::OntologyClassEdit.Properties.Resources.cahiers_de_labo;
            this.ToolStripSplitButton_Graph.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripSplitButton_Graph.Name = "ToolStripSplitButton_Graph";
            this.ToolStripSplitButton_Graph.Size = new System.Drawing.Size(31, 20);
            this.ToolStripSplitButton_Graph.Text = "ToolStripSplitButton1";
            this.ToolStripSplitButton_Graph.ButtonClick += new System.EventHandler(this.ToolStripSplitButton_Graph_ButtonClick);
            // 
            // ToolStripMenuItem_ClassAttributes
            // 
            this.ToolStripMenuItem_ClassAttributes.CheckOnClick = true;
            this.ToolStripMenuItem_ClassAttributes.Name = "ToolStripMenuItem_ClassAttributes";
            this.ToolStripMenuItem_ClassAttributes.Size = new System.Drawing.Size(209, 22);
            this.ToolStripMenuItem_ClassAttributes.Text = "x_Class-Attributes";
            this.ToolStripMenuItem_ClassAttributes.CheckStateChanged += new System.EventHandler(this.ToolStripMenuItem_ClassAttributes_CheckStateChanged);
            // 
            // ClassRelationsforwToolStripMenuItem
            // 
            this.ClassRelationsforwToolStripMenuItem.CheckOnClick = true;
            this.ClassRelationsforwToolStripMenuItem.Name = "ClassRelationsforwToolStripMenuItem";
            this.ClassRelationsforwToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.ClassRelationsforwToolStripMenuItem.Text = "x_Class-Relations (forw)";
            this.ClassRelationsforwToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.ClassRelationsforwToolStripMenuItem_CheckStateChanged);
            // 
            // ClassRelationsbackwToolStripMenuItem
            // 
            this.ClassRelationsbackwToolStripMenuItem.CheckOnClick = true;
            this.ClassRelationsbackwToolStripMenuItem.Name = "ClassRelationsbackwToolStripMenuItem";
            this.ClassRelationsbackwToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.ClassRelationsbackwToolStripMenuItem.Text = "x_Class-Relations (backw)";
            this.ClassRelationsbackwToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.ClassRelationsbackwToolStripMenuItem_CheckStateChanged);
            // 
            // ClassRelationsORToolStripMenuItem
            // 
            this.ClassRelationsORToolStripMenuItem.CheckOnClick = true;
            this.ClassRelationsORToolStripMenuItem.Name = "ClassRelationsORToolStripMenuItem";
            this.ClassRelationsORToolStripMenuItem.Size = new System.Drawing.Size(209, 22);
            this.ClassRelationsORToolStripMenuItem.Text = "x_Class-Relations (OR)";
            this.ClassRelationsORToolStripMenuItem.CheckStateChanged += new System.EventHandler(this.ClassRelationsORToolStripMenuItem_CheckStateChanged);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel_ClassLBL,
            this.ToolStripTextBox_Name,
            this.ToolStripSeparator1,
            this.ToolStripLabel_GUIDLBL,
            this.ToolStripTextBox_GUID,
            this.ToolStripButton_DelClass});
            this.ToolStrip1.Location = new System.Drawing.Point(3, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(639, 25);
            this.ToolStrip1.TabIndex = 0;
            // 
            // ToolStripLabel_ClassLBL
            // 
            this.ToolStripLabel_ClassLBL.Name = "ToolStripLabel_ClassLBL";
            this.ToolStripLabel_ClassLBL.Size = new System.Drawing.Size(47, 22);
            this.ToolStripLabel_ClassLBL.Text = "x_Class:";
            // 
            // ToolStripTextBox_Name
            // 
            this.ToolStripTextBox_Name.Name = "ToolStripTextBox_Name";
            this.ToolStripTextBox_Name.Size = new System.Drawing.Size(250, 25);
            this.ToolStripTextBox_Name.TextChanged += new System.EventHandler(this.ToolStripTextBox_Name_TextChanged);
            // 
            // ToolStripSeparator1
            // 
            this.ToolStripSeparator1.Name = "ToolStripSeparator1";
            this.ToolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // ToolStripLabel_GUIDLBL
            // 
            this.ToolStripLabel_GUIDLBL.Name = "ToolStripLabel_GUIDLBL";
            this.ToolStripLabel_GUIDLBL.Size = new System.Drawing.Size(47, 22);
            this.ToolStripLabel_GUIDLBL.Text = "x_GUID:";
            // 
            // ToolStripTextBox_GUID
            // 
            this.ToolStripTextBox_GUID.Name = "ToolStripTextBox_GUID";
            this.ToolStripTextBox_GUID.ReadOnly = true;
            this.ToolStripTextBox_GUID.Size = new System.Drawing.Size(250, 25);
            this.ToolStripTextBox_GUID.TextChanged += new System.EventHandler(this.ToolStripTextBox_GUID_TextChanged);
            // 
            // ToolStripButton_DelClass
            // 
            this.ToolStripButton_DelClass.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton_DelClass.Image = global::OntologyClassEdit.Properties.Resources.tasto_8_architetto_franc_01;
            this.ToolStripButton_DelClass.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton_DelClass.Name = "ToolStripButton_DelClass";
            this.ToolStripButton_DelClass.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton_DelClass.Text = "ToolStripButton1";
            this.ToolStripButton_DelClass.Click += new System.EventHandler(this.ToolStripButton_DelClass_Click);
            // 
            // UserControl_ClassEdit
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolStripContainer1);
            this.Controls.Add(this.StatusStrip1);
            this.Name = "UserControl_ClassEdit";
            this.Size = new System.Drawing.Size(848, 558);
            this.StatusStrip1.ResumeLayout(false);
            this.StatusStrip1.PerformLayout();
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.LeftToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.LeftToolStripPanel.PerformLayout();
            this.ToolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.SplitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SplitContainer1)).EndInit();
            this.SplitContainer1.ResumeLayout(false);
            this.TabControl1.ResumeLayout(false);
            this.ToolStrip2.ResumeLayout(false);
            this.ToolStrip2.PerformLayout();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        internal System.Windows.Forms.StatusStrip StatusStrip1;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel_DatabaseLBL;
        internal System.Windows.Forms.ToolStripStatusLabel ToolStripStatusLabel_Database;
        internal System.Windows.Forms.ToolStripContainer ToolStripContainer1;
        internal System.Windows.Forms.SplitContainer SplitContainer1;
        internal System.Windows.Forms.TabControl TabControl1;
        internal System.Windows.Forms.TabPage TabPage_Forward;
        internal System.Windows.Forms.TabPage TabPage_Backward;
        internal System.Windows.Forms.TabPage TabPage_ObjectReferences;
        internal System.Windows.Forms.ToolStrip ToolStrip2;
        internal System.Windows.Forms.ToolStripSplitButton ToolStripSplitButton_Graph;
        internal System.Windows.Forms.ToolStripMenuItem ToolStripMenuItem_ClassAttributes;
        internal System.Windows.Forms.ToolStripMenuItem ClassRelationsforwToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ClassRelationsbackwToolStripMenuItem;
        internal System.Windows.Forms.ToolStripMenuItem ClassRelationsORToolStripMenuItem;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel_ClassLBL;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox_Name;
        internal System.Windows.Forms.ToolStripSeparator ToolStripSeparator1;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel_GUIDLBL;
        internal System.Windows.Forms.ToolStripTextBox ToolStripTextBox_GUID;
        internal System.Windows.Forms.ToolStripButton ToolStripButton_DelClass;
    }
}
