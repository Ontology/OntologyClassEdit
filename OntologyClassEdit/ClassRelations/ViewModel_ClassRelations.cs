﻿using OntologyAppDBConnector;
using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.Converter;
using OntologyClassEdit.Translations;
using OntologyClasses.BaseClasses;
using OntologyViewModels.BaseClasses;
using OntologyViewModels.DataAdapter;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.ClassRelations
{
    
    public class ViewModel_ClassRelations : OntologyClassEdit.BaseClasses.ViewModelBase
    {

        public ListType ListType { get; set; }

        public List<clsClassRel> ClassRelations
        {
            get
            {
                if (ListType == ListType.ClassClassRelation_Conscious)
                {
                    return ViewModelOitemList.ListAdapter.ClassRelConciousViewItems.Select(clsItem => clsItem.GetProxyItem()).ToList();
                }
                else if (ListType == ListType.ClassClassRelation_Subconscious)
                {
                    return ViewModelOitemList.ListAdapter.ClassRelSubconsciousViewItems.Select(clsItem => clsItem.GetProxyItem()).ToList();

                }
                else if (ListType == ListType.ClassOtherRelation_Conscious)
                {
                    return ViewModelOitemList.ListAdapter.ClassRelConciousViewItems.Select(clsItem => clsItem.GetProxyItem()).ToList();
                }
                else
                {
                    return null;
                }
                
            }
        }

        private clsOntologyItem classItemForRelation;
        public clsOntologyItem ClassItemForRelation
        {
            get { return classItemForRelation; }
            set
            {
                classItemForRelation = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_ClassItemForRelation);
            }
        }

        private clsOntologyItem relationTypeItemForRelation;
        public clsOntologyItem RelationTypeItemForRelation
        {
            get { return relationTypeItemForRelation; }
            set
            {
                relationTypeItemForRelation = value;
                RaisePropertyChanged(NotifyChanges.ClassRelation_RelationTypeItemForRelation);
            }
        }

        public clsOntologyItem SaveRelation()
        {
            var result = globals.LState_Nothing.Clone();

            var addAdapter = ViewModelOitemList.GetAddFormItemType();
            var oItemClass = ViewModelOitemList.ListAdapter.OItem(ListType == ListType.ClassClassRelation_Conscious || ListType == ListType.ClassOtherRelation_Conscious ? ViewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Left : ViewModelOitemList.ListAdapter.FilterItemClassRel.ID_Class_Right, globals.Type_Class);

            if (ListType == ListType.ClassClassRelation_Conscious || ListType == ListType.ClassClassRelation_Subconscious)
            {
                addAdapter.TypeApplied1 = ClassItemForRelation.Type;
                addAdapter.ResultItems_OntologyEditor1 = new List<clsOntologyItem> { ClassItemForRelation };
                addAdapter.TypeApplied2 = RelationTypeItemForRelation.Type;
                addAdapter.ResultItems_OntologyEditor2 = new List<clsOntologyItem> { RelationTypeItemForRelation };
            }
            else if (ListType == ListType.ClassOtherRelation_Conscious)
            {
                addAdapter.TypeApplied1 = RelationTypeItemForRelation.Type;
                addAdapter.ResultItems_OntologyEditor1 = new List<clsOntologyItem> { RelationTypeItemForRelation };
            }
            
            
            

            if (oItemClass != null && addAdapter.CheckOk())
            {
                result = ListType == ListType.ClassClassRelation_Conscious ? addAdapter.Add_ClassClass_Conscious(oItemClass) :
                         ListType == ListType.ClassClassRelation_Subconscious ? addAdapter.Add_ClassClass_Subconscious(oItemClass) :
                         addAdapter.Add_ClassOther(oItemClass);
                if (result.GUID == globals.LState_Error.GUID)
                {
                    ViewModelErrors |= ClassRelationsViewModelErrors.SaveItems;
                }
            }
            else
            {
                ViewModelErrors |= NonNameErrorToClassRelationsViewModelError.convert(addAdapter.NonNameErrorType);
                result = globals.LState_Error.Clone();
            }

            return result;
        }

        public ViewModel_ClassRelations(Globals globals) : base(globals)
        {
        }

        public DeleteCounter DelClassRelations()
        {
            var result = ViewModelOitemList.DelClassRelations();
            if (result.CountError != 0 || result.CountRelated != 0)
            {
                result.ErrorCaption = TranslationManager.Caption_DeleteError;
                result.ErrorMessage = string.Format(TranslationManager.Message_DeleteError, result.CountToDo, result.CountDone, result.CountError, result.CountRelated);
            }
            return result;
        }
    }
}
