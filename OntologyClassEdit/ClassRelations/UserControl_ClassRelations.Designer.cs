﻿namespace OntologyClassEdit.ClassRelations
{
    partial class UserControl_ClassRelations
    {
        /// <summary> 
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Komponenten-Designer generierter Code

        /// <summary> 
        /// Erforderliche Methode für die Designerunterstützung. 
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.ToolStripContainer1 = new System.Windows.Forms.ToolStripContainer();
            this.ToolStrip1 = new System.Windows.Forms.ToolStrip();
            this.ToolStripLabel_CountLBL = new System.Windows.Forms.ToolStripLabel();
            this.ToolStripLabel_Count = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel_List = new System.Windows.Forms.ToolStripLabel();
            this.DataGridView_Relation = new System.Windows.Forms.DataGridView();
            this.ToolStrip2 = new System.Windows.Forms.ToolStrip();
            this.ToolStripButton_Add = new System.Windows.Forms.ToolStripButton();
            this.ToolStripButton_Del = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton_SetMinForwTo0 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_DecMinForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_IncMinForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_EditMinForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton_SetMaxForwToInvinite = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_DecMaxForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_IncMaxForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_EditMaxForw = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton_EditMaxBackw = new System.Windows.Forms.ToolStripButton();
            this.ToolStripContainer1.BottomToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.ContentPanel.SuspendLayout();
            this.ToolStripContainer1.TopToolStripPanel.SuspendLayout();
            this.ToolStripContainer1.SuspendLayout();
            this.ToolStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Relation)).BeginInit();
            this.ToolStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // ToolStripContainer1
            // 
            // 
            // ToolStripContainer1.BottomToolStripPanel
            // 
            this.ToolStripContainer1.BottomToolStripPanel.Controls.Add(this.ToolStrip1);
            // 
            // ToolStripContainer1.ContentPanel
            // 
            this.ToolStripContainer1.ContentPanel.Controls.Add(this.DataGridView_Relation);
            this.ToolStripContainer1.ContentPanel.Size = new System.Drawing.Size(622, 440);
            this.ToolStripContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ToolStripContainer1.Location = new System.Drawing.Point(0, 0);
            this.ToolStripContainer1.Name = "ToolStripContainer1";
            this.ToolStripContainer1.Size = new System.Drawing.Size(622, 490);
            this.ToolStripContainer1.TabIndex = 2;
            this.ToolStripContainer1.Text = "ToolStripContainer1";
            // 
            // ToolStripContainer1.TopToolStripPanel
            // 
            this.ToolStripContainer1.TopToolStripPanel.Controls.Add(this.ToolStrip2);
            // 
            // ToolStrip1
            // 
            this.ToolStrip1.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripLabel_CountLBL,
            this.ToolStripLabel_Count,
            this.toolStripSeparator1,
            this.toolStripLabel_List});
            this.ToolStrip1.Location = new System.Drawing.Point(3, 0);
            this.ToolStrip1.Name = "ToolStrip1";
            this.ToolStrip1.Size = new System.Drawing.Size(100, 25);
            this.ToolStrip1.TabIndex = 0;
            // 
            // ToolStripLabel_CountLBL
            // 
            this.ToolStripLabel_CountLBL.Name = "ToolStripLabel_CountLBL";
            this.ToolStripLabel_CountLBL.Size = new System.Drawing.Size(53, 22);
            this.ToolStripLabel_CountLBL.Text = "x_Count:";
            // 
            // ToolStripLabel_Count
            // 
            this.ToolStripLabel_Count.Name = "ToolStripLabel_Count";
            this.ToolStripLabel_Count.Size = new System.Drawing.Size(13, 22);
            this.ToolStripLabel_Count.Text = "0";
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel_List
            // 
            this.toolStripLabel_List.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripLabel_List.Image = global::OntologyClassEdit.Properties.Resources.tasto_8_architetto_franc_01;
            this.toolStripLabel_List.Name = "toolStripLabel_List";
            this.toolStripLabel_List.Size = new System.Drawing.Size(16, 22);
            this.toolStripLabel_List.Text = "toolStripLabel1";
            // 
            // DataGridView_Relation
            // 
            this.DataGridView_Relation.AllowUserToAddRows = false;
            this.DataGridView_Relation.AllowUserToDeleteRows = false;
            this.DataGridView_Relation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridView_Relation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DataGridView_Relation.Location = new System.Drawing.Point(0, 0);
            this.DataGridView_Relation.Name = "DataGridView_Relation";
            this.DataGridView_Relation.ReadOnly = true;
            this.DataGridView_Relation.Size = new System.Drawing.Size(622, 440);
            this.DataGridView_Relation.TabIndex = 1;
            // 
            // ToolStrip2
            // 
            this.ToolStrip2.Dock = System.Windows.Forms.DockStyle.None;
            this.ToolStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ToolStripButton_Add,
            this.ToolStripButton_Del,
            this.toolStripSeparator2,
            this.toolStripLabel1,
            this.toolStripButton_SetMinForwTo0,
            this.toolStripButton_DecMinForw,
            this.toolStripButton_IncMinForw,
            this.toolStripButton_EditMinForw,
            this.toolStripSeparator3,
            this.toolStripLabel2,
            this.toolStripButton_SetMaxForwToInvinite,
            this.toolStripButton_DecMaxForw,
            this.toolStripButton_IncMaxForw,
            this.toolStripButton_EditMaxForw,
            this.toolStripButton_EditMaxBackw});
            this.ToolStrip2.Location = new System.Drawing.Point(3, 0);
            this.ToolStrip2.Name = "ToolStrip2";
            this.ToolStrip2.ShowItemToolTips = false;
            this.ToolStrip2.Size = new System.Drawing.Size(371, 25);
            this.ToolStrip2.TabIndex = 0;
            // 
            // ToolStripButton_Add
            // 
            this.ToolStripButton_Add.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton_Add.Image = global::OntologyClassEdit.Properties.Resources.b_plus;
            this.ToolStripButton_Add.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton_Add.Name = "ToolStripButton_Add";
            this.ToolStripButton_Add.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton_Add.Text = "ToolStripButton1";
            this.ToolStripButton_Add.Click += new System.EventHandler(this.ToolStripButton_Add_Click);
            // 
            // ToolStripButton_Del
            // 
            this.ToolStripButton_Del.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.ToolStripButton_Del.Image = global::OntologyClassEdit.Properties.Resources.b_minus;
            this.ToolStripButton_Del.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.ToolStripButton_Del.Name = "ToolStripButton_Del";
            this.ToolStripButton_Del.Size = new System.Drawing.Size(23, 22);
            this.ToolStripButton_Del.Text = "ToolStripButton2";
            this.ToolStripButton_Del.Click += new System.EventHandler(this.ToolStripButton_Del_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(31, 22);
            this.toolStripLabel1.Text = "Min:";
            // 
            // toolStripButton_SetMinForwTo0
            // 
            this.toolStripButton_SetMinForwTo0.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_SetMinForwTo0.Image = global::OntologyClassEdit.Properties.Resources.b_down_0;
            this.toolStripButton_SetMinForwTo0.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SetMinForwTo0.Name = "toolStripButton_SetMinForwTo0";
            this.toolStripButton_SetMinForwTo0.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_SetMinForwTo0.Text = "toolStripButton1";
            this.toolStripButton_SetMinForwTo0.Click += new System.EventHandler(this.toolStripButton_SetMinForwTo0_Click);
            // 
            // toolStripButton_DecMinForw
            // 
            this.toolStripButton_DecMinForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DecMinForw.Image = global::OntologyClassEdit.Properties.Resources.b_down_minus;
            this.toolStripButton_DecMinForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DecMinForw.Name = "toolStripButton_DecMinForw";
            this.toolStripButton_DecMinForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DecMinForw.Text = "toolStripButton1";
            this.toolStripButton_DecMinForw.Click += new System.EventHandler(this.toolStripButton_DecMinForw_Click);
            // 
            // toolStripButton_IncMinForw
            // 
            this.toolStripButton_IncMinForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_IncMinForw.Image = global::OntologyClassEdit.Properties.Resources.b_down_plus;
            this.toolStripButton_IncMinForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_IncMinForw.Name = "toolStripButton_IncMinForw";
            this.toolStripButton_IncMinForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_IncMinForw.Text = "toolStripButton3";
            this.toolStripButton_IncMinForw.Click += new System.EventHandler(this.toolStripButton_IncMinForw_Click);
            // 
            // toolStripButton_EditMinForw
            // 
            this.toolStripButton_EditMinForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_EditMinForw.Image = global::OntologyClassEdit.Properties.Resources.bb_txt_;
            this.toolStripButton_EditMinForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_EditMinForw.Name = "toolStripButton_EditMinForw";
            this.toolStripButton_EditMinForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_EditMinForw.Text = "toolStripButton5";
            this.toolStripButton_EditMinForw.Click += new System.EventHandler(this.toolStripButton_EditMinForw_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(32, 22);
            this.toolStripLabel2.Text = "Max:";
            // 
            // toolStripButton_SetMaxForwToInvinite
            // 
            this.toolStripButton_SetMaxForwToInvinite.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_SetMaxForwToInvinite.Image = global::OntologyClassEdit.Properties.Resources.b_up_Infinite;
            this.toolStripButton_SetMaxForwToInvinite.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_SetMaxForwToInvinite.Name = "toolStripButton_SetMaxForwToInvinite";
            this.toolStripButton_SetMaxForwToInvinite.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_SetMaxForwToInvinite.Text = "toolStripButton1";
            this.toolStripButton_SetMaxForwToInvinite.Click += new System.EventHandler(this.toolStripButton_SetMaxForwToInvinite_Click);
            // 
            // toolStripButton_DecMaxForw
            // 
            this.toolStripButton_DecMaxForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_DecMaxForw.Image = global::OntologyClassEdit.Properties.Resources.b_up_minus;
            this.toolStripButton_DecMaxForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_DecMaxForw.Name = "toolStripButton_DecMaxForw";
            this.toolStripButton_DecMaxForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_DecMaxForw.Text = "toolStripButton4";
            this.toolStripButton_DecMaxForw.Click += new System.EventHandler(this.toolStripButton_DecMaxForw_Click);
            // 
            // toolStripButton_IncMaxForw
            // 
            this.toolStripButton_IncMaxForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_IncMaxForw.Image = global::OntologyClassEdit.Properties.Resources.b_up_plus;
            this.toolStripButton_IncMaxForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_IncMaxForw.Name = "toolStripButton_IncMaxForw";
            this.toolStripButton_IncMaxForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_IncMaxForw.Text = "toolStripButton2";
            this.toolStripButton_IncMaxForw.Click += new System.EventHandler(this.toolStripButton_IncMaxForw_Click);
            // 
            // toolStripButton_EditMaxForw
            // 
            this.toolStripButton_EditMaxForw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_EditMaxForw.Image = global::OntologyClassEdit.Properties.Resources.bb_txt_;
            this.toolStripButton_EditMaxForw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_EditMaxForw.Name = "toolStripButton_EditMaxForw";
            this.toolStripButton_EditMaxForw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_EditMaxForw.Text = "toolStripButton6";
            this.toolStripButton_EditMaxForw.Click += new System.EventHandler(this.toolStripButton_EditMaxForw_Click);
            // 
            // toolStripButton_EditMaxBackw
            // 
            this.toolStripButton_EditMaxBackw.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton_EditMaxBackw.Image = global::OntologyClassEdit.Properties.Resources.bb_txt_maxbackw;
            this.toolStripButton_EditMaxBackw.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton_EditMaxBackw.Name = "toolStripButton_EditMaxBackw";
            this.toolStripButton_EditMaxBackw.Size = new System.Drawing.Size(23, 22);
            this.toolStripButton_EditMaxBackw.Text = "toolStripButton1";
            this.toolStripButton_EditMaxBackw.Click += new System.EventHandler(this.toolStripButton_EditMaxBackw_Click);
            // 
            // UserControl_ClassRelations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.ToolStripContainer1);
            this.Name = "UserControl_ClassRelations";
            this.Size = new System.Drawing.Size(622, 490);
            this.ToolStripContainer1.BottomToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.BottomToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ContentPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.ResumeLayout(false);
            this.ToolStripContainer1.TopToolStripPanel.PerformLayout();
            this.ToolStripContainer1.ResumeLayout(false);
            this.ToolStripContainer1.PerformLayout();
            this.ToolStrip1.ResumeLayout(false);
            this.ToolStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridView_Relation)).EndInit();
            this.ToolStrip2.ResumeLayout(false);
            this.ToolStrip2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        internal System.Windows.Forms.ToolStripContainer ToolStripContainer1;
        internal System.Windows.Forms.ToolStrip ToolStrip1;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel_CountLBL;
        internal System.Windows.Forms.ToolStripLabel ToolStripLabel_Count;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel_List;
        internal System.Windows.Forms.DataGridView DataGridView_Relation;
        internal System.Windows.Forms.ToolStrip ToolStrip2;
        internal System.Windows.Forms.ToolStripButton ToolStripButton_Add;
        internal System.Windows.Forms.ToolStripButton ToolStripButton_Del;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripButton toolStripButton_SetMinForwTo0;
        private System.Windows.Forms.ToolStripButton toolStripButton_DecMinForw;
        private System.Windows.Forms.ToolStripButton toolStripButton_IncMinForw;
        private System.Windows.Forms.ToolStripButton toolStripButton_EditMinForw;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.ToolStripButton toolStripButton_SetMaxForwToInvinite;
        private System.Windows.Forms.ToolStripButton toolStripButton_DecMaxForw;
        private System.Windows.Forms.ToolStripButton toolStripButton_IncMaxForw;
        private System.Windows.Forms.ToolStripButton toolStripButton_EditMaxForw;
        private System.Windows.Forms.ToolStripButton toolStripButton_EditMaxBackw;
    }
}
