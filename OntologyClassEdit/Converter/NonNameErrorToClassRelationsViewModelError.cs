﻿using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.ClassAttributes;
using OntologyViewModels.DataAdapter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.Converter
{
    public static class NonNameErrorToClassRelationsViewModelError
    {
        public static ClassRelationsViewModelErrors convert(NonNameErrorType nonNameErrorType)
        {
            if (nonNameErrorType.HasFlag(NonNameErrorType.Type1IsWrong))
            {
                return ClassRelationsViewModelErrors.TypeOrCount;
            }
            else if (nonNameErrorType.HasFlag(NonNameErrorType.Count1))
            {
                return ClassRelationsViewModelErrors.TypeOrCount;
            }
            else
            {
                return ClassRelationsViewModelErrors.None;
            }
        }
    }
}
