﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using OntologyViewModels.OItemList;
using OntologyViewModels.DataAdapter;

namespace OntologyClassEdit.Converter
{
    public enum ImageType
    {
        NotLoaded = 0,
        Loaded = 1
    }
    public static class LoadStateToImageConverter
    {
        public static ImageType ConvertListFilterStateToImageType(ListLoadStateItem listLoadState )
        {
            
            if (listLoadState.HasFlag(ListLoadStateItem.ListLoadLoaded))
            {
                return ImageType.Loaded;
            }
            return ImageType.NotLoaded;
        }

        public static Image ConvertImageTypeToImage(ImageType imageType)
        {

            if (imageType == ImageType.Loaded)
            {
                return Properties.Resources.ListLoadedImage;
            }
            return Properties.Resources.ListLoadingPending;
        }

    }
}
