﻿using OntologyClassEdit.BaseClasses;
using OntologyClassEdit.ClassAttributes;
using OntologyViewModels.OItemList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OntologyClassEdit.Converter
{
    public static class ViewModelErrorTypeToClassRelationsError
    {
        public static ClassRelationsViewModelErrors Convert(OItemListViewModelErrorType oItemListViewModelErrorType)
        {
            var result = ClassRelationsViewModelErrors.None;

            if (oItemListViewModelErrorType.HasFlag(OItemListViewModelErrorType.SaveError))
            {
                result |= ClassRelationsViewModelErrors.SaveError;
            }
            else if (oItemListViewModelErrorType.HasFlag(OItemListViewModelErrorType.SelectionCountError))
            {
                result |= ClassRelationsViewModelErrors.TypeOrCount;
            }
            else if (oItemListViewModelErrorType.HasFlag(OItemListViewModelErrorType.WrongItemType))
            {
                result |= ClassRelationsViewModelErrors.WrongItemType;
            }
            else if (oItemListViewModelErrorType.HasFlag(OItemListViewModelErrorType.MinChangeNotValid))
            {
                result |= ClassRelationsViewModelErrors.MinForwChangeNotValid;
            }
            else if (oItemListViewModelErrorType.HasFlag(OItemListViewModelErrorType.MaxChangeNotValid))
            {
                result |= ClassRelationsViewModelErrors.MaxForwChangeNotValid;
            }

            return result;
        }
    }
}